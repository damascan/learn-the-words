import React from "react";
import ReactDOM from "react-dom";
import s from "./index.module.css";
import {ReactComponent as ReactLogo} from './logo.svg'

const App = () => {
  return (
    <div className={s.cover}>
      <HeaderBlock />
    </div>
  );
};
const HeaderBlock = () => {
  return (
    <div className={s.header}>
      <h1>Успешный Успех</h1>
      <p>Так,я бросил курить прочитав книгу </p>
      <p> "лёгкий способ бросить курить".</p>
      <ReactLogo />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
